//
//  CreditsViewController.swift
//  Little
//
//  Created by Anil on 22/12/15.
//  Copyright © 2015 ThatSoft. All rights reserved.
//

import UIKit

class CreditsViewController: UIViewController {

    @IBOutlet weak var quoraLabel: TTTAttributedLabel!
    @IBOutlet weak var factsSiteLabel: TTTAttributedLabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let color = UIColor.littleGreen()
        quoraLabel.linkAttributes = [kCTForegroundColorAttributeName : color]
        quoraLabel.enabledTextCheckingTypes = NSTextCheckingType.Link.rawValue
        quoraLabel.text = "www.thefactsite.com"
        factsSiteLabel.linkAttributes = [kCTForegroundColorAttributeName : color]
        factsSiteLabel.enabledTextCheckingTypes = NSTextCheckingType.Link.rawValue
        factsSiteLabel.text = "www.quora.com"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    func attributedLabel(label: TTTAttributedLabel!, didSelectLinkWithURL url: NSURL!) {
        loadURL(url)
    }
    
    func loadURL(url: NSURL){
        let browser = TSMiniWebBrowser(url: url)
        browser.showPageTitleOnTitleBar = false;
        browser.mode = TSMiniWebBrowserModeModal;
        self.presentViewController(browser, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
