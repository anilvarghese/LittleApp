//
//  AppDelegate.swift
//  LittleFacts
//
//  Created by Anil on 21/11/15.
//  Copyright © 2015 ThatSoft. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UITabBarControllerDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        Fact.registerSubclass()
        Parse.enableLocalDatastore()
        Parse.setApplicationId(ParseConfig.applicationId, clientKey: ParseConfig.clientKey)
        PFUser.enableAutomaticUser()
        PFUser.currentUser()!.incrementKey("RunCount")
        PFUser.currentUser()!.saveInBackground()
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)

        setAppearanceForHudView()
        FBSDKAppEvents.activateApp()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        //DataManager.sharedInstance.setInAppPurchased()
        Appodeal.initializeWithApiKey("3fb29872d0e005b6715071d4841c6fced3c371a1b1ddaf96",types: [.Banner,.Interstitial])
        
        //Load data
        let rootVC = window?.rootViewController as? UITabBarController
        rootVC?.delegate = self
        let homeVC = rootVC?.viewControllers?.first as? HomeViewController
        homeVC?.loadData()
        
        return true
    }
    
    internal func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController){
        
        if let vc = viewController as? HomeViewController{
            vc.loadData()
        }
    }
    
    func requestPushNotificationAccess() {
        
        let application = UIApplication.sharedApplication()
        
        let types:UIUserNotificationType = ([.Alert, .Badge, .Sound])
        let settings:UIUserNotificationSettings = UIUserNotificationSettings(forTypes: types, categories: nil)
        
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
    }

    func setAppearanceForHudView(){
        RZHud.appearance().hudColor = UIColor.clearColor()//UIColor(red: 255/255.0, green: 188/255.0, blue: 22/255.0, alpha: 0.0428)
        RZHud.appearance().labelColor = UIColor.clearColor()//UIColor(red: 255/255.0, green: 188/255.0, blue: 22/255.0, alpha: 0.0428)
        
        RZHud.appearance().overlayColor = UIColor.clearColor()//UIColor(red: 255/255.0, green: 188/255.0, blue: 22/255.0, alpha: 0.0428)
        
        RZHud.appearance().shadowAlpha = 0.0
        RZHud.appearance().spinnerColor = UIColor.redColor()
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        // Store the deviceToken in the current Installation and save it to Parse
        let installation = PFInstallation.currentInstallation()
        installation.setDeviceTokenFromData(deviceToken)
        installation["user"] = PFUser.currentUser()
        installation.saveInBackground()
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        delay(1) { () -> () in
            self.requestPushNotificationAccess()
            
        }
        
        let currentInstallation = PFInstallation.currentInstallation()
        if currentInstallation.badge != 0 {
            currentInstallation.badge = 0
            currentInstallation.saveEventually()
        }
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
         return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }


}

