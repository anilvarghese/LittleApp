//
//  HomeViewController.swift
//  LittleFacts
//
//  Created by Anil on 22/11/15.
//  Copyright © 2015 ThatSoft. All rights reserved.
//

import UIKit
import Koloda
import pop

private let numberOfCards: UInt = 5
private let frameAnimationSpringBounciness:CGFloat = 9
private let frameAnimationSpringSpeed:CGFloat = 16
private let kolodaCountOfVisibleCards = 1
private let kolodaAlphaValueSemiTransparent:CGFloat = 0


class HomeViewController: UIViewController, KolodaViewDataSource, KolodaViewDelegate,FBAdViewDelegate {

    @IBOutlet weak var kolodaView: CustomKolodaView!
    @IBOutlet weak var adContainerView: UIView!
    @IBOutlet weak var adContainerHeight: NSLayoutConstraint!
    
    var allFacts = [Fact]()
    var limit = 100
    var skip = 0
    var adView:FBAdView!
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        kolodaView.alphaValueSemiTransparent = kolodaAlphaValueSemiTransparent
        kolodaView.countOfVisibleCards = kolodaCountOfVisibleCards
        kolodaView.dataSource = self
        kolodaView.delegate = self

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //In App purchase
        let isInAppPurchased = DataManager.sharedInstance.isInAppPurchased()
        
        if isInAppPurchased{
            adContainerHeight.constant = 0
            self.adContainerView.hidden = true
            Appodeal.hideBanner()

        }else{
            let banner = Appodeal.banner()
            banner.frame = adContainerView.bounds
            adContainerView.addSubview(banner)

        }

    }
    
    func loadData(){
        if NetworkReachability.isConnectedToNetwork(){
            fetchData()
            
        }else{
            showCachedData()
            
        }
    }
    
    func loadAdView(){
        adView = FBAdView(placementID: "1629318264000895_1629554657310589", adSize: kFBAdSizeHeight50Banner, rootViewController: self)
        adView.delegate = self
        adView.loadAd()
        adContainerView.addSubview(adView)
        adView.frame = adContainerView.bounds
    }

    
    func showCachedData(){
        let query = PFQuery(className:"Fact")
        query.fromLocalDatastore()
        query.limit = limit
        query.skip = skip
        query.orderByDescending("createdAt")
        
        self.showHUD()
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            self.hideHUD()
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let _objects = objects as? [Fact] where objects?.count>0{
                    for object in _objects {
                        print(object.objectId)
                        //TODO: do it more elegantly
                        object.isLiked = DataManager.sharedInstance.isLikedFact(object)
                        
                    }
                    self.allFacts = _objects
                    self.kolodaView.resetCurrentCardNumber()
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
    }
    
    func fetchData(){
        
        let query = PFQuery(className:"Fact")
        query.limit = limit
        query.skip = skip
        query.orderByDescending("createdAt")
        
        self.showHUD()
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            self.hideHUD()
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let _objects = objects as? [Fact] where objects?.count>0{
                    for object in _objects {
                        print(object.objectId)
                        //TODO: do it more elegantly
                        object.isLiked = DataManager.sharedInstance.isLikedFact(object)

                    }
                    self.allFacts = _objects.shuffle()
                }
                
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
            
            self.kolodaView.resetCurrentCardNumber()
        }
    }
    
    //MARK: IBActions
    @IBAction func leftButtonTapped() {
        kolodaView?.swipe(SwipeResultDirection.Left)
    }
    
    @IBAction func rightButtonTapped() {
        kolodaView?.swipe(SwipeResultDirection.Right)
    }
    
    @IBAction func refreshButtonTapped() {
        // Refresh data
        
        skip = 0
        fetchData()
    }
    
    @IBAction func shareButtonTapped(sender: AnyObject) {
        
        let kolodaView = kolodaViewForCardAtIndex(self.kolodaView, index: UInt(self.kolodaView.currentCardNumber)) as? CardView
        
        if let shareView = kolodaView{
            shareView.appNameLabel.hidden = false
            shareView.favouriteButton.hidden = true
            UIGraphicsBeginImageContext(shareView.bounds.size)
            shareView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
            shareView.layer.cornerRadius = 0
            let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            
            // let's add a String and an NSURL
            let activityViewController = UIActivityViewController(
                activityItems: [image,ShareCard(),],
                applicationActivities: nil)
//            activityViewController.completionHandler = {
//                (item: String?, completed: Bool) in
//            }
            
            self.presentViewController(activityViewController, animated: true, completion:nil)
            
        }
    }
    func activityViewControllerPlaceholderItem(activityViewController: UIActivityViewController) -> AnyObject{
        
     return "Do you know?"
    }
    
    func activityViewController(activityViewController: UIActivityViewController, itemForActivityType activityType: String) -> AnyObject?{
        
        let kolodaView = kolodaViewForCardAtIndex(self.kolodaView, index: UInt(self.kolodaView.currentCardNumber)) as? CardView
        
        if let shareView = kolodaView{

        shareView.appNameLabel.hidden = false
        shareView.favouriteButton.hidden = true
        UIGraphicsBeginImageContext(shareView.bounds.size)
        shareView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        shareView.layer.cornerRadius = 0
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let someText:String = "Do you know?"
            
            return  [someText, image]
        }

        return nil
    }
    
    //MARK: KolodaViewDataSource
    func kolodaNumberOfCards(koloda: KolodaView) -> UInt {
        return UInt(allFacts.count)
    }
    
    func kolodaViewForCardAtIndex(koloda: KolodaView, index: UInt) -> UIView {
        
        let fact = allFacts[Int(index)]
        
        let view = NSBundle.mainBundle().loadNibNamed("CardView",
            owner: self, options: nil)[0] as! CardView
        view.imageView.image =  UIImage(named: "BG\(index % 5)")
        view.imageView.layer.cornerRadius = 15
        view.imageView.layer.masksToBounds = true
        
        view.descriptionLabel.text = fact.title // fact["description"] as? String
        view.fact = fact
        view.setFavouriteButtonForFact(fact)
        view.appNameLabel.hidden = true
        view.layoutIfNeeded()
        
        print("Types: \(fact.tags)")
        view.typeLabel.text = fact.tags.first

        
        if index == 15{
            Appodeal.showAd(.Interstitial, rootViewController: self)
        }
        return view

    }
    func kolodaViewForCardOverlayAtIndex(koloda: KolodaView, index: UInt) -> OverlayView? {
        return NSBundle.mainBundle().loadNibNamed("CustomOverlayView",
            owner: self, options: nil)[0] as? OverlayView
    }

    //MARK: KolodaViewDelegate
    
    func kolodaDidSwipedCardAtIndex(koloda: KolodaView, index: UInt, direction: SwipeResultDirection) {
    }
    
    func kolodaDidRunOutOfCards(koloda: KolodaView) {
        //Example: reloading
        skip = skip + limit
        fetchData()
    }
    
    func kolodaDidSelectCardAtIndex(koloda: KolodaView, index: UInt) {
    }
    
    func kolodaShouldApplyAppearAnimation(koloda: KolodaView) -> Bool {
        return true
    }
    
    func kolodaShouldMoveBackgroundCard(koloda: KolodaView) -> Bool {
        return false
    }
    
    func kolodaShouldTransparentizeNextCard(koloda: KolodaView) -> Bool {
        return false
    }
    
    func kolodaBackgroundCardAnimation(koloda: KolodaView) -> POPPropertyAnimation? {
        let animation = POPSpringAnimation(propertyNamed: kPOPViewFrame)
        animation.springBounciness = frameAnimationSpringBounciness
        animation.springSpeed = frameAnimationSpringSpeed
        return animation
    }
    
    func createImageToShareForMessage(message:String)->UIImage {
        
        let image = UIImage(named: "BG\(0)")!
        
        let imageWidth = image.size.width
        
        let imageHeight = image.size.height
        let widthOffsets = CGFloat(20)
        
        let topOffSet = CGFloat(100)
        let bottomOffset = CGFloat(20)
        
        let maxLabelheight = imageHeight - (topOffSet + bottomOffset)
        let labelWidth = imageWidth - CGFloat(2*widthOffsets)
        
        let attrs = [NSFontAttributeName: UIFont(name: "Avenir-Roman", size: 30.0)!]
        
        //        var sizeOfLabel = self.topicObject.message.boundingRectWithSize(CGSizeMake(imageWidth, maxLabelheight), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes:[NSFontAttributeName: UIFont(name: "Avenir-Light", size: 48.0)] ,  context:nil).size
        
        let sizeOfLabel = message.boundingRectWithSize(CGSizeMake(imageWidth, maxLabelheight), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attrs, context: nil).size
        
        let calCulatedHeightOfLabel = sizeOfLabel.height
        
        //        var positionY = imageHeight - calCulatedHeightOfLabel - bottomOffset//CGFloat(topOffSet) + (maxLabelheight - calCulatedHeightOfLabel)/2
        
        let positionY = CGFloat(topOffSet) + (maxLabelheight - calCulatedHeightOfLabel)/2
        //        var imageToShare = Utility.drawText(self.topicObject.message, image: UIImage(named: imagesArray[currentIndex])!, point: CGPointMake(10, 200))
        
        let imageToShare = drawTextInFrame(message, image: image, frame: CGRectMake(widthOffsets, positionY, labelWidth, calCulatedHeightOfLabel))
        
        //        var imageWithVotePoints = Utility.drawText("\(self.topicObject.voteCount)", image: imageToShare, point: CGPointMake(imageToShare.size.width - 100 , 30))
        return imageToShare
    }
     func drawTextInFrame(text: String, image:UIImage, frame: CGRect) -> UIImage {
        let font: UIFont = UIFont(name: "Avenir-Roman", size: 30)!
        UIGraphicsBeginImageContext(image.size)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        
        let style: NSMutableParagraphStyle = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        style.alignment = NSTextAlignment.Center
        //        var style2 = NSParagraphStyle()
        //        style.alignment = .Center
        
        
        let textFontAttributes = [
            NSFontAttributeName: font,
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSParagraphStyleAttributeName:style
        ]
        let rect = frame
        UIColor.whiteColor().set()
        text.drawInRect(rect, withAttributes: textFontAttributes)
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }

}
