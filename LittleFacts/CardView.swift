//
//  CardView.swift
//  LittleFacts
//
//  Created by Anil on 22/11/15.
//  Copyright © 2015 ThatSoft. All rights reserved.
//

import UIKit

class CardView: UIView {

    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    var fact:Fact!
    @IBOutlet weak var typeLabel: UILabel!
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    func setFavouriteButtonForFact(fact:Fact){
        
        if fact.isLiked{
            favouriteButton.setImage(UIImage(named: "For You"), forState: .Normal)
        }else{
            favouriteButton.setImage(UIImage(named: "For You Empty"), forState: .Normal)
        }
    }
    
    @IBAction func favouriteButtonTapped(sender: UIButton) {
        
        let isLiked = DataManager.sharedInstance.isLikedFact(fact)
        
        if !isLiked{
            favouriteButton.setImage(UIImage(named: "For You"), forState: .Normal)
            DataManager.sharedInstance.likeFact(fact)
            
            var likesCount = fact.likes //fact["likes"] as? Int ?? 0
            likesCount++
            fact.likes = likesCount
            let relation = fact.relationForKey("likedBy")
            relation.addObject(PFUser.currentUser()!)

            fact.saveInBackground()
        }else{
            favouriteButton.setImage(UIImage(named: "For You Empty"), forState: .Normal)
            DataManager.sharedInstance.unLikeFact(fact)
            
            var likesCount = fact.likes //["likes"] as? Int
            if likesCount > 0{
                likesCount--
                fact.likes = likesCount
                let relation = fact.relationForKey("likedBy")
                relation.removeObject(PFUser.currentUser()!)
                fact.saveInBackground()
            }

        }
    }

}
