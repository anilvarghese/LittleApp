//
//  Fact.swift
//  LittleFacts
//
//  Created by Anil on 22/11/15.
//  Copyright © 2015 ThatSoft. All rights reserved.
//

import UIKit
import Parse

class Fact : PFObject, PFSubclassing {
    
    @NSManaged var title: String
    @NSManaged var factDescription: String
    @NSManaged var reference: String
    @NSManaged var likes: Int
    @NSManaged var isLiked: Bool
    @NSManaged var tags: Array<String>
    
    
    override class func initialize() {
        struct Static {
            static var onceToken : dispatch_once_t = 0;
        }
        dispatch_once(&Static.onceToken) {
            self.registerSubclass()
        }
    }
    
    static func parseClassName() -> String {
        return "Fact"
    }
}