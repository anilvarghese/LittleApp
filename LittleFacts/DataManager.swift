//
//  DataManager.swift
//  LittleFacts
//
//  Created by Anil on 22/11/15.
//  Copyright © 2015 ThatSoft. All rights reserved.
//

import UIKit

class DataManager: NSObject {
    
    static let sharedInstance = DataManager()
    private var defaults = NSUserDefaults.standardUserDefaults()
    
    func shouldShowAds()->Bool{
        return true
    }
    
    func isLikedFact(fact:Fact)-> Bool{
        let liked = defaults.boolForKey("little-facts-\(fact.objectId!)-liked?")
        return liked
    }
    
    func likeFact(fact:Fact){
        fact.isLiked = true
        defaults.setBool(true, forKey: "little-facts-\(fact.objectId!)-liked?")
        defaults.synchronize()
    }
    
    func unLikeFact(fact:Fact){
        fact.isLiked = true
        defaults.setBool(false, forKey: "little-facts-\(fact.objectId!)-liked?")
        defaults.synchronize()
    }
    
    func setInAppPurchased(){
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "LittleAppPurchased")
    }
    
    func isInAppPurchased()->Bool{
       let isPurchased = NSUserDefaults.standardUserDefaults().boolForKey("LittleAppPurchased")
        return isPurchased
    }
}
