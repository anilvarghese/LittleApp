//
//  FavouritesViewController.swift
//  LittleFacts
//
//  Created by Anil on 22/11/15.
//  Copyright © 2015 ThatSoft. All rights reserved.
//

import UIKit

class FavouritesViewController: HomeViewController {

    @IBOutlet weak var favouriteLabel: UILabel!
    @IBOutlet weak var favIcon: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func showCachedData(){
        let query = PFQuery(className:"Fact")
        query.fromLocalDatastore()
        query.limit = limit
        query.skip = skip
        query.whereKey("likedBy", equalTo: PFUser.currentUser()!)
        
        self.showHUD()
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            self.hideHUD()
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) objects.")
                // Do something with the found objects
                if let _objects = objects as? [Fact] where objects?.count>0{
                    for object in _objects {
                        print(object.objectId)
                        //TODO: do it more elegantly
                        object.isLiked = DataManager.sharedInstance.isLikedFact(object)
                        
                    }
                    self.allFacts = _objects
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
            self.kolodaView.resetCurrentCardNumber()
        }

    }
    
    override func fetchData(){
        
        let query = PFQuery(className:"Fact")
        query.limit = limit
        query.skip = skip
        query.whereKey("likedBy", equalTo: PFUser.currentUser()!)
        query.orderByDescending("updatedAt")
        
        self.showHUD()
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            self.hideHUD()
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) objects.")
                // Do something with the found objects
                if let _objects = objects as? [Fact] where objects?.count>0{
                    for object in _objects {
                        print(object.objectId)
                        //TODO: do it more elegantly
                        object.isLiked = DataManager.sharedInstance.isLikedFact(object)
                        
                    }
                    self.allFacts = _objects
                }else{
                    self.favouriteLabel.hidden = false
                    self.favIcon.hidden = false
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
            self.kolodaView.resetCurrentCardNumber()

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
