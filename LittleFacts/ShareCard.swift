//
//  ShareCard.swift
//  Little
//
//  Created by Anil on 09/01/16.
//  Copyright © 2016 ThatSoft. All rights reserved.
//

import UIKit

public class ShareCard: NSObject,UIActivityItemSource {
    
    var title: String = ""
    var url = "https://goo.gl/4WoBYB"
    
    public func activityViewControllerPlaceholderItem(activityViewController: UIActivityViewController) -> AnyObject{
        
        print("Place holder")
        return title
    }
    
    public func activityViewController(activityViewController: UIActivityViewController, itemForActivityType activityType: String) -> AnyObject? {
       
        print("Place holder itemForActivity")
        
        return "Little App (\(url))"
    }
    
    public func activityViewController(activityViewController: UIActivityViewController, subjectForActivityType activityType: String?) -> String{
        
        print("Place holder subjectForActivity")
        return "Did you know?"
    }
    
}
