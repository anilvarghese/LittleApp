//
//  DisableAdsViewController.swift
//  Little
//
//  Created by Anil on 22/12/15.
//  Copyright © 2015 ThatSoft. All rights reserved.
//

import UIKit

class DisableAdsViewController: UIViewController {

    private var inAppProduct:SKProduct?
    @IBOutlet weak var inPurchaseLabel: UILabel!
    @IBOutlet weak var alreadyPurchasedLabel: UILabel!
    
    @IBOutlet weak var thankyouLabel: UILabel!
    @IBOutlet weak var enjoyLabel: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var restoreButton: UIButton!
    
    // priceFormatter is used to show proper, localized currency
    lazy var priceFormatter: NSNumberFormatter = {
        let pf = NSNumberFormatter()
        pf.formatterBehavior = .Behavior10_4
        pf.numberStyle = .CurrencyStyle
        return pf
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewForPurchase(true)
        let isPurchased = DataManager.sharedInstance.isInAppPurchased()
        if isPurchased == false{
            //Lets try to load from server
            loadInAppPurchase()
        }
        // Subscribe to a notification that fires when a product is purchased.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "productPurchased:", name: IAPHelperProductPurchasedNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func setupViewForPurchase(purchased:Bool){
        alreadyPurchasedLabel.hidden = purchased
        buyButton.hidden = purchased
        restoreButton.hidden = purchased
        thankyouLabel.hidden = !purchased
        enjoyLabel.hidden = !purchased
    }
    
    func loadInAppPurchase(){
        RageProducts.store.requestProductsWithCompletionHandler { success, products in
            if success && products.count > 0 {
                self.inAppProduct = products[0]
                self.checkForPurchaseForProduct(self.inAppProduct!)
            }
        }
        
    }
    
    // Purchase the product
    @IBAction func buyButtonTapped(sender: AnyObject) {
        RageProducts.store.purchaseProduct(self.inAppProduct!)
    }
    
    
    @IBAction func restoreButtonTapped(sender: AnyObject) {
        // Restore purchases to this device.
        RageProducts.store.restoreCompletedTransactions()
    }
    
    // When a product is purchased, this notification fires, redraw the correct row
    func productPurchased(notification: NSNotification) {
        //        let productIdentifier = notification.object as! String
        DataManager.sharedInstance.setInAppPurchased()
        setupViewForPurchase(true)
        //Hide buy button & restore
    }
    
    func checkForPurchaseForProduct(product: SKProduct){
        if RageProducts.store.isProductPurchased(product.productIdentifier) {
            DataManager.sharedInstance.setInAppPurchased()
            setupViewForPurchase(true)
        }
        else if IAPHelper.canMakePayments() {
            priceFormatter.locale = product.priceLocale
            let price = priceFormatter.stringFromNumber(product.price)
            
            setupViewForPurchase(false)
            buyButton.setTitle("Disable Ads: \(price!)", forState: .Normal)
            
        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
