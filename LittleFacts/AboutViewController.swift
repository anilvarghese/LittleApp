//
//  AboutViewController.swift
//  LittleFacts
//
//  Created by Anil on 22/11/15.
//  Copyright © 2015 ThatSoft. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController,TTTAttributedLabelDelegate {

    @IBOutlet weak var linkText: TTTAttributedLabel!
    @IBOutlet weak var versionLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let color = UIColor.littleGreen()
        linkText.linkAttributes = [kCTForegroundColorAttributeName : color]
        linkText.enabledTextCheckingTypes = NSTextCheckingType.Link.rawValue
        linkText.text = "www.little.thatsoft.com"
        versionLabel.text = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as! String

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func attributedLabel(label: TTTAttributedLabel!, didSelectLinkWithURL url: NSURL!) {
        loadURL(url)
    }

    func loadURL(url: NSURL){
        let browser = TSMiniWebBrowser(url: url)
        browser.showPageTitleOnTitleBar = false;
        browser.mode = TSMiniWebBrowserModeModal;
        self.presentViewController(browser, animated: true, completion: nil)
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
