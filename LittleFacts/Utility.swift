//
//  Utility.swift
//  Little
//
//  Created by Anil on 20/12/15.
//  Copyright © 2015 ThatSoft. All rights reserved.
//

import UIKit

class Utility: NSObject {

}

extension CollectionType {
    /// Return a copy of `self` with its elements shuffled
    func shuffle() -> [Generator.Element] {
        var list = Array(self)
        list.shuffleInPlace()
        return list
    }
}

extension MutableCollectionType where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffleInPlace() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in 0..<count - 1 {
            let j = Int(arc4random_uniform(UInt32(count - i))) + i
            guard i != j else { continue }
            swap(&self[i], &self[j])
        }
    }
}

extension UIColor{
    class func littleGreen()->UIColor{
        return UIColor(red: 0.0, green: 209.0/255.0, blue: 119.0/255.0, alpha: 0.95)
    }
}