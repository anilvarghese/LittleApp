//
//  NativeAdView.h
//  LittleFacts
//
//  Created by Anil on 24/11/15.
//  Copyright © 2015 ThatSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
@import FBAudienceNetwork;


@interface NativeAdView : UIView<FBNativeAdDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) FBNativeAd *nativeAd;
@property (strong, nonatomic) IBOutlet UIImageView *adIconImageView;
@property (weak, nonatomic) IBOutlet FBMediaView *adCoverMediaView;
@property (strong, nonatomic) IBOutlet UILabel *adTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *adBodyLabel;
@property (strong, nonatomic) IBOutlet UIButton *adCallToActionButton;
@property (strong, nonatomic) IBOutlet UILabel *adSocialContextLabel;
@property (strong, nonatomic) IBOutlet UILabel *sponsoredLabel;

@end
