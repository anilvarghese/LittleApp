//
//  TrendingViewController.swift
//  LittleFacts
//
//  Created by Anil on 22/11/15.
//  Copyright © 2015 ThatSoft. All rights reserved.
//

import UIKit

class TrendingViewController: HomeViewController {

//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        // Do any additional setup after loading the view.
//        
//    }
    
    override func showCachedData(){
        let query = PFQuery(className:"Fact")
        query.fromLocalDatastore()
        query.limit = limit
        query.skip = skip
        query.orderByDescending("likes")
        
        self.showHUD()
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            self.hideHUD()
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let _objects = objects as? [Fact] where objects?.count>0{
                    for object in _objects {
                        print(object.objectId)
                        //TODO: do it more elegantly
                        object.isLiked = DataManager.sharedInstance.isLikedFact(object)
                        
                    }
                    self.allFacts = _objects
                    self.kolodaView.reloadData()
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }

    }

    override func fetchData(){
        
        let query = PFQuery(className:"Fact")
        query.limit = limit
        query.skip = skip
        query.orderByDescending("likes")
        query.orderByDescending("updatedAt")
        
        self.showHUD()
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            self.hideHUD()
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let _objects = objects as? [Fact] where objects?.count>0{
                    for object in _objects {
                        print(object.objectId)
                        //TODO: do it more elegantly
                        object.isLiked = DataManager.sharedInstance.isLikedFact(object)

                    }
                    self.allFacts = _objects
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
            self.kolodaView.resetCurrentCardNumber()
        }
    }

}
