//
//  NativeAdView.m
//  LittleFacts
//
//  Created by Anil on 24/11/15.
//  Copyright © 2015 ThatSoft. All rights reserved.
//

#import "NativeAdView.h"

@interface NativeAdView ()


@end


@implementation NativeAdView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if(self =   [super initWithCoder:aDecoder]){
        
        [self loadNativeAd];

    }
    return self;
}


- (void)loadNativeAd
{
    
    // Create a native ad request with a unique placement ID (generate your own on the Facebook app settings).
    // Use different ID for each ad placement in your app.
    FBNativeAd *nativeAd = [[FBNativeAd alloc] initWithPlacementID:@"1629318264000895_1629554657310589"];
    
    // Set a delegate to get notified when the ad was loaded.
    nativeAd.delegate = self;
    
    // When testing on a device, add its hashed ID to force test ads.
    // The hash ID is printed to console when running on a device.
    // [FBAdSettings addTestDevice:@"THE HASHED ID AS PRINTED TO CONSOLE"];
    
    // Initiate a request to load an ad.
    dispatch_async(dispatch_get_main_queue(), ^{

        [nativeAd loadAd];

    });

}

#pragma mark - FBNativeAdDelegate implementation

- (void)nativeAdDidLoad:(FBNativeAd *)nativeAd
{
    NSLog(@"Native ad was loaded, constructing native UI...");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
    
    if (self.nativeAd) {
        [self.nativeAd unregisterView];
    }
    
    self.nativeAd = nativeAd;
    
    // Create native UI using the ad metadata.
    [self.adCoverMediaView setNativeAd:nativeAd];
    
    __weak typeof(self) weakSelf = self;
    [self.nativeAd.icon loadImageAsyncWithBlock:^(UIImage *image) {
        __strong typeof(self) strongSelf = weakSelf;
        strongSelf.adIconImageView.image = image;
    }];
    
    // Render native ads onto UIView
    self.adTitleLabel.text = self.nativeAd.title;
    self.adBodyLabel.text = self.nativeAd.body;
    self.adSocialContextLabel.text = self.nativeAd.socialContext;
    self.sponsoredLabel.text = @"Sponsored";
    
    [self setCallToActionButton:self.nativeAd.callToAction];
    
    NSLog(@"Register UIView for impression and click...");
    
    // Wire up UIView with the native ad; the whole UIView will be clickable.
    [nativeAd registerViewForInteraction:self
                      withViewController:[[[UIApplication sharedApplication] keyWindow] rootViewController]];
        
    });
    
    // Or you can replace above call with following function, so you can specify the clickable areas.
    // NSArray *clickableViews = @[self.adCallToActionButton, self.adCoverMediaView];
    // [nativeAd registerViewForInteraction:self.adUIView
    //                   withViewController:self
    //                   withClickableViews:clickableViews];
    
}

- (void)nativeAd:(FBNativeAd *)nativeAd didFailWithError:(NSError *)error
{
    NSLog(@"Native ad failed to load with error: %@", error);
}

- (void)nativeAdDidClick:(FBNativeAd *)nativeAd
{
    NSLog(@"Native ad was clicked.");
}

- (void)nativeAdDidFinishHandlingClick:(FBNativeAd *)nativeAd
{
    NSLog(@"Native ad did finish click handling.");
}

- (void)nativeAdWillLogImpression:(FBNativeAd *)nativeAd
{
    NSLog(@"Native ad impression is being captured.");
}

#pragma mark - Private Methods

- (void)setCallToActionButton:(NSString *)callToAction
{
    [self.adCallToActionButton setHidden:NO];
    [self.adCallToActionButton setTitle:callToAction
                               forState:UIControlStateNormal];
}

@end
